//
//  TokenTests.swift
//  TokenGame
//
//  Created by Pawel Dabrowski on 02/06/2017.
//  Copyright © 2017 Lightcoding. All rights reserved.
//

import XCTest
@testable import TokenGame

class TokenTests: XCTestCase {
    
    func testTokenIsInitiallyClosed(){
        
        let token = Token(isWinning: false)
        XCTAssertEqual(token.opened, false)
    }
    
    func testWiningTokenIsInitiallyClosed(){
        
        let token = Token(isWinning: true)
        XCTAssertEqual(token.opened, false)
    }
    
    func testOpenResultOpened(){
        
        let token = Token(isWinning: false)
        XCTAssertEqual(token.open(), Token.OpenResult.isOpened)
    }
    
    func testOpenResultIsAlreadyOpened(){
        
        let token = Token(isWinning: false)
        
        let _ = token.open()
        
        XCTAssertEqual(token.open(), Token.OpenResult.isAlreadyOpened)
    }
    
    func testOpenResultIsWinning(){
        
        let token = Token(isWinning: true)
        XCTAssertEqual(token.open(), Token.OpenResult.isWinning)
    }
}
