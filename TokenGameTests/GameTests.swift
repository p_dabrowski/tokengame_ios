//
//  GameTests.swift
//  TokenGame
//
//  Created by Pawel Dabrowski on 02/06/2017.
//  Copyright © 2017 Lightcoding. All rights reserved.
//

import XCTest
@testable import TokenGame

class GameTests: XCTestCase {
    
    let configuration = GameConfiguration()

    func testGameStateIsInitiallyPlaying(){
        
        let game = Game(configuration: configuration, delegate: nil)
        XCTAssertEqual(game.gameStatus, .playing)
    }

    func testGameInitiallyHaveTokens(){
        
        let game = Game(configuration: configuration, delegate: nil)
        XCTAssertEqual(game.tokens.count, configuration.tokensCount())
    }
    
    func testIsOnlyOneWinningToken(){
        
        let game = Game(configuration: configuration, delegate: nil)
        
        var winningTokensCount = 0
        for i in 0...(configuration.tokensCount() - 1){
            if game.tokens[i].isWinning{
                winningTokensCount += 1
            }
        }
        
        XCTAssertEqual(winningTokensCount, 1)
    }
    
    func testGameStatusNumberOfRowsExceeded(){
        let game = Game(configuration: configuration, delegate: nil)
        
        game.round = configuration.maxRoundsCount
        let _ = game.open(tokenNumber: 0)
        XCTAssertEqual(game.gameStatus, .numberOfRoundsExceeded)
    }
    
    func testGameStatusVictory(){
        let game = Game(configuration: configuration, delegate: nil)
        
        for i in 0...(configuration.tokensCount()-1){
            if game.tokens[i].isWinning{
                let _ = game.open(tokenNumber: i)
                break
            }
        }
        XCTAssertEqual(game.gameStatus, .victory)
    }

    func testGameStatusTimeIsOver() {
        let game = Game(configuration: configuration, delegate: nil)
        game.startDate = Date(timeIntervalSinceNow: ( -1 * (configuration.maxTimeInSeconds + 1)) )
        let _ = game.open(tokenNumber: 0)
        XCTAssertEqual(game.gameStatus, .timeIsOver)
    }

    func testRoundNotChangedAfterOpeningOpenedToken(){
        let game = Game(configuration: configuration, delegate: nil)
        
        for i in 0...(configuration.tokensCount()-1){
            if game.tokens[i].isWinning == false{
                
                
                let _ = game.open(tokenNumber: i)
                let _ = game.open(tokenNumber: i)
                let _ = game.open(tokenNumber: i)
                let _ = game.open(tokenNumber: i)

                break
            }
        }
        XCTAssertEqual(game.round, 1)
    }
    
    func testGetRowsCount() {
        let game = Game(configuration: configuration, delegate: nil)
        XCTAssertEqual(game.getRowsCount(), configuration.rows)
    }
    
    func testGetColumnsCount() {
        let game = Game(configuration: configuration, delegate: nil)
        XCTAssertEqual(game.getColumnsCount(), configuration.columns)
    }
}
