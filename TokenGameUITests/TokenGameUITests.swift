//
//  TokenGameUITests.swift
//  TokenGameUITests
//
//  Created by Pawel Dabrowski on 02/06/2017.
//  Copyright © 2017 Lightcoding. All rights reserved.
//

import XCTest

class TokenGameUITests: XCTestCase {
    
    var app: XCUIApplication!

    override func setUp() {
        super.setUp()
        
        continueAfterFailure = false
        app = XCUIApplication()
        app.launch()
    }
    
    func testGameFinishedAfter5Rounds(){
        let startButton = app.buttons["start"]
        startButton.tap()
        
        let firstToken  = app.collectionViews.children(matching:.any).element(boundBy: 0)
        if firstToken.exists {
            firstToken.tap()
        }
        let secondToken  = app.collectionViews.children(matching:.any).element(boundBy: 1)
        if secondToken.exists {
            secondToken.tap()
        }
        let thirdToken  = app.collectionViews.children(matching:.any).element(boundBy: 2)
        if thirdToken.exists {
            thirdToken.tap()
        }
        let fourthToken  = app.collectionViews.children(matching:.any).element(boundBy: 3)
        if fourthToken.exists {
            fourthToken.tap()
        }
        let fifthToken  = app.collectionViews.children(matching:.any).element(boundBy: 4)
        if fifthToken.exists {
            fifthToken.tap()
        }

        let timeIsOverLabel = app.staticTexts["Time is over"]
        let roundsExceededLabel = app.staticTexts["Number of rounds exceeded"]
        let winLabel = app.staticTexts["Congratulations, You win!"]

        XCTAssertTrue(timeIsOverLabel.exists || roundsExceededLabel.exists || winLabel.exists)
    }
    
    func testTimeIsOver() {
        
        let startButton = app.buttons["start"]
        startButton.tap()
        
        let firstToken = app.collectionViews.children(matching:.any).element(boundBy: 0)
        if firstToken.exists {
            firstToken.tap()
        }
        
        sleep(65)
        
        let statusLabel = app.staticTexts["Time is over"]
        
        XCTAssertTrue(statusLabel.exists)
    }
}
