//
//  ViewController.swift
//  TokenGame
//
//  Created by Pawel Dabrowski on 02/06/2017.
//  Copyright © 2017 Lightcoding. All rights reserved.
//

import UIKit

class ViewController: UIViewController {
    
    @IBOutlet weak var statusLabel: UILabel!
    @IBOutlet weak var collectionView: UICollectionView!
    
    var game: GameInterface?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupCollectionView()
        setupStatusLabel(gameStatus: .playing)
    }
    
    func setupCollectionView() {
        self.collectionView?.backgroundColor = .white
        self.collectionView?.register(UICollectionViewCell.self, forCellWithReuseIdentifier: "TokenCell")
        self.collectionView.delegate = self
        self.collectionView.dataSource = self
    }
    
    @IBAction func startButtonAction(_ sender: Any) {
        let configuration = GameConfiguration()
        game = Game(configuration: configuration, delegate: self)
        collectionView.reloadData()
        setupStatusLabel(gameStatus: .playing)
    }
    
    func getTokenIndex(indexPath : IndexPath) -> Int{
        print("\(indexPath.row + 1) \(indexPath.section + 1)")
        
        guard let game = game else {
            return 0
        }
        
        return indexPath.section * game.getColumnsCount() + indexPath.row
    }
    
    func setupStatusLabel(gameStatus: GameStatus) {
        statusLabel.text = gameStatus.title()
        statusLabel.isHidden = gameStatus == .playing
    }
}

extension ViewController: GameDelegate{
    
    func gameStatusChanged(gameStatus: GameStatus){
        
        setupStatusLabel(gameStatus: gameStatus)
        
        if gameStatus != .playing{
            let actionSheetController: UIAlertController = UIAlertController(title: "", message: gameStatus.alertMessage(), preferredStyle: .alert)
            let cancelAction: UIAlertAction = UIAlertAction(title: "Ok", style: .cancel)
            actionSheetController.addAction(cancelAction)
            self.present(actionSheetController, animated: true, completion: nil)
        }
    }
}

extension ViewController: UICollectionViewDataSource{
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        guard let game = game else {
            return 0
        }
        
        return game.getColumnsCount()
    }
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        guard let game = game else {
            return 0
        }
        
        return game.getRowsCount()
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let index = getTokenIndex(indexPath: indexPath)
        return game!.getToken(index: index)!.collectionView(collectionView, cellForItemAt: indexPath)
    }
}

extension ViewController: UICollectionViewDelegate{
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        guard let game = game else {
            return
        }
        
        let _ = game.open(tokenNumber: getTokenIndex(indexPath: indexPath))
        
        DispatchQueue.main.async {
            collectionView.reloadItems(at: [indexPath])
        }
    }
}
