//
//  Game.swift
//  TokenGame
//
//  Created by Pawel Dabrowski on 02/06/2017.
//  Copyright © 2017 Lightcoding. All rights reserved.
//

import UIKit

protocol GameDelegate{
    func gameStatusChanged(gameStatus: GameStatus)
}

protocol GameInterface {
    init(configuration: GameConfiguration, delegate: GameDelegate?)
    func open(tokenNumber: Int) -> Token?
    func getToken(index: Int) -> Token?
    func getColumnsCount() -> Int
    func getRowsCount() -> Int
}

enum GameStatus{
    case playing
    case timeIsOver
    case numberOfRoundsExceeded
    case victory
    
    func alertMessage() -> String? {
        switch self {
        case  .playing:
            break
        case .timeIsOver:
            return "Time is over"
        case .numberOfRoundsExceeded:
            return "Number of rounds exceeded"
        case .victory:
            return "Congratulations, You win!"
        }
        return nil
    }
    
    func title() -> String? {
        return alertMessage()
    }
}

class Game: GameInterface{
    
    var configuration: GameConfiguration!
    var delegate: GameDelegate?
    var round = 0
    var startDate: Date!
    var tokens = [Token]()
    var timer: Timer?
    
    
    private var _gameStatus = GameStatus.playing
    var gameStatus: GameStatus {
        get{
            return _gameStatus
        }
        set (newVal){
            if newVal != _gameStatus{
                _gameStatus = newVal
                delegate?.gameStatusChanged(gameStatus: _gameStatus)
                
            }
            if newVal != .playing{
                timer?.invalidate()
            }
        }
    }
    
    required init(configuration: GameConfiguration, delegate: GameDelegate?) {
        startDate = Date()
        self.delegate = delegate
        self.configuration = configuration
        timer = Timer.scheduledTimer(timeInterval: configuration.maxTimeInSeconds, target: self, selector: #selector(Game.timerExpirationHandler), userInfo: nil, repeats: false)
        
        createTokens()
    }
}

extension Game{ // private methods
    
    @objc fileprivate func timerExpirationHandler() {
        gameStatus = .timeIsOver
    }
    
    fileprivate func createTokens(){
        
        tokens = [Token]()
        
        let winningTokenNumber = generateWinningTokenNumber()
        
        for i in 0...(configuration.tokensCount()-1){
            tokens.append(Token(isWinning: i == winningTokenNumber))
        }
    }
    
    fileprivate func generateWinningTokenNumber() -> Int {
        return Int(arc4random()) % configuration.tokensCount()
    }
    
    fileprivate func checkIfRoundsExceeded() -> Bool{
        return round >= configuration.maxRoundsCount
    }
    
    fileprivate func tokensCount() -> Int{
        return configuration.tokensCount()
    }
}

extension Game{ // GameInterface methods
    
    internal func getRowsCount() -> Int {
        return configuration.rows
    }
    
    internal func getColumnsCount() -> Int {
        return configuration.columns
    }
    
    internal func open(tokenNumber: Int) -> Token?{
        
        guard gameStatus == .playing else {
            return nil
        }
        
        if abs(startDate.timeIntervalSinceNow) > configuration.maxTimeInSeconds {
            
            gameStatus = .timeIsOver
            return nil
        }
        
        let token = tokens[tokenNumber]
        let openResult = token.open()
        
        switch openResult {
        case Token.OpenResult.isOpened:
            round += 1
            
            if checkIfRoundsExceeded(){
                gameStatus = .numberOfRoundsExceeded
                return nil
            }
            
            gameStatus = .playing
            return token
        case Token.OpenResult.isAlreadyOpened:
            gameStatus = .playing
            return token
        case Token.OpenResult.isWinning:
            gameStatus = .victory
            return token
        }
    }
    
    internal func getToken(index: Int) -> Token? {
        if index>=0 && tokens.count > index{
            return tokens[index]
        }
        return nil
    }
}
