//
//  Token.swift
//  TokenGame
//
//  Created by Pawel Dabrowski on 02/06/2017.
//  Copyright © 2017 Lightcoding. All rights reserved.
//

import UIKit

class Token{
    
    private(set) var opened = false
    let isWinning : Bool
    
    init(isWinning: Bool){
        self.isWinning = isWinning
    }
    
    enum OpenResult {
        case isOpened
        case isAlreadyOpened
        case isWinning
    }
    
    func open() -> OpenResult{
        
        if isWinning {
            opened = true
            return .isWinning
        }
        
        if opened {
            return .isAlreadyOpened
        }
        
        opened = true
        return .isOpened
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "TokenCell", for: indexPath)
        
        if opened{
            cell.backgroundColor = isWinning ? .yellow : .green
        }
        else{
            cell.backgroundColor =  .red
        }
        return cell
    }
}
