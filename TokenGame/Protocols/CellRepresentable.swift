//
//  CellRepresentable.swift
//  TokenGame
//
//  Created by Pawel Dabrowski on 06/06/2017.
//  Copyright © 2017 Lightcoding. All rights reserved.
//

import UIKit

protocol CellRepresentable{
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell
    
}
