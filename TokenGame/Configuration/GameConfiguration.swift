//
//  GameConfiguration.swift
//  TokenGame
//
//  Created by Pawel Dabrowski on 06/06/2017.
//  Copyright © 2017 Lightcoding. All rights reserved.
//

import UIKit

struct GameConfiguration {
    let rows = 4
    let columns = 5
    let maxTimeInSeconds: Double = 60
    let maxRoundsCount = 5
    
    func tokensCount() -> Int{
        return rows * columns
    }
}
